﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController : ControllerBase
    {
        private readonly IRepository<Employee> _employeeRepository;
        private readonly IRepository<Role> _roleRepository;

        public EmployeesController(IRepository<Employee> employeeRepository, IRepository<Role> roleRepository)
        {
            _employeeRepository = employeeRepository;
            _roleRepository = roleRepository;
        }

        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<EmployeeShortResponse>> GetEmployeesAsync()
        {
            var employees = await _employeeRepository.GetAllAsync();

            var employeesModelList = employees.Select(x => 
                new EmployeeShortResponse()
                    {
                        Id = x.Id,
                        Email = x.Email,
                        FullName = x.FullName,
                    }).ToList();

            return employeesModelList;
        }
        
        /// <summary>
        /// Получить данные сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();
            
            var employeeModel = new EmployeeResponse()
            {
                Id = employee.Id,
                Email = employee.Email,
                Roles = employee.Roles.Select(x => new RoleItemResponse()
                {
                    Name = x.Name,
                    Description = x.Description
                }).ToList(),
                FullName = employee.FullName,
                AppliedPromocodesCount = employee.AppliedPromocodesCount
            };

            return employeeModel;
        }

        /// <summary>
        /// Создать нового сотрудника
        /// </summary>
        /// <param name="employee"> Представление сотрудника </param>
        /// <returns>Добавленная запись</returns>
        [HttpPost]
        public async Task<ActionResult<Employee>> CreateEmployeeAsync(EmployeeEdit employee)
        {
            var roleModel = await _roleRepository.GetAllAsync();
            var roles = new List<Role>();
            foreach (var id in employee.Roles)
            {
                roles.Add(roleModel.First(x => x.Id == id));
            }

            var employeeModel = new Employee()
            {
                FirstName = employee.FirstName,
                LastName = employee.LastName,
                Roles = roles,
                Email = employee.Email,
                AppliedPromocodesCount = employee.AppliedPromocodesCount
            };
            //var emploe = await _employeeRepository.CreateAsync(employeeModel);
            //return  emploe;
            return await _employeeRepository.CreateAsync(employeeModel);

        }

        /// <summary>
        /// Изменение данных о сотруднике
        /// </summary>
        /// <param name="id">Guid записи сотрудника </param>
        /// <param name="employee">Полное представление сотрудника </param>
        /// <returns></returns>
        [HttpPut("{id:guid}")]
        public async Task<IActionResult> UpdateEmployeeAsync(Guid id, EmployeeEdit employee)
        {
            //проверяем id в строке запроса на соотвествие с id элемента. 
            //if (id != employee.Id)
            //{
            //    return BadRequest();
            //}
            var role = new List<Role>();
            foreach (var x in employee.Roles)
            {
                var t = await _roleRepository.GetByIdAsync(x);
                role.Add(t);
            }

            var item = _employeeRepository.GetByIdAsync(id);
            if (item == null)
                return NotFound();
            var empl = new Employee()
            {
                Id = id,
                FirstName = employee.FirstName,
                LastName = employee.LastName,
                Email = employee.Email,
                Roles = role,
                AppliedPromocodesCount = employee.AppliedPromocodesCount
            };
            await _employeeRepository.UpdateAsync(empl);

            return await Task.FromResult(Ok());
        }


        /// <summary>
        /// Удаление сотрудника
        /// </summary>
        /// <param name="id"> Guid записи сотрудника</param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<IActionResult> DeleteEmployeeAsync(Guid id)
        {
            var employee = _employeeRepository.GetByIdAsync(id);
            if (employee == null)
            {
                return NotFound();
            }
            //return NoContent();

            await _employeeRepository.DeleteAsync(id);
       
            return await Task.FromResult(Ok());
        }


    }
}