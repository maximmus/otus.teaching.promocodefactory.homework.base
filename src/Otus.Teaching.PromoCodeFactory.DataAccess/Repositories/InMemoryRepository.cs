﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T: BaseEntity
    {
        protected static IEnumerable<T> Data { get; set; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = data;
        }
        
        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Data);
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }


        /// <summary>
        /// Создание записи сотрудника
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public async Task<T> CreateAsync(T data)
        {
            data.Id = Guid.NewGuid();
            ((IList<T>)Data).Add(data);
            return await Task.FromResult(data);
        }

        /// <summary>
        /// Изменение данных сотрудника по ID
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public async Task<T> UpdateAsync(T data)
        {
            var index = ((IList<T>)Data).IndexOf(Data.Where(x => x.Id == data.Id).First());
            ((IList<T>)Data)[index] = data;

            return await Task.FromResult(data);
        }


        /// <summary>
        /// Удаление записи сотрудника по ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Task DeleteAsync(Guid id)
        {
            ((List<T>)Data).RemoveAll(i => i.Id == id);
            return Task.CompletedTask;
        }

    }
}